=======================
Secure Kernel Execution
=======================

Secure Kernel Execution Environment, or SKEEN as it's referenced within kernel
source, is a mechanism that allows kernel space code to interact with Intel(R)
Software Guard Extension (SGX). SGX is known for allowing only accesses from
the least privileged execution level, ring 3, allowing the kernel code only to
perform operations related to creation and management of their "secure
worlds".

Because of that, SKEEN architecture is expected to have at least two separated
components: kernel code, which wills for using TEEs functionality, and
userspace code, which acts as a middleware for kernel operations. Each of
these are better detailed in their respective documentation.

This service can be configured on by enabling:

    "Security options"/"Enable kernel access to Intel Software Guard Extension (SGX)"
    (CONFIG_SKEEN)

In depth documentation for major architecture components are present for:

..toctree::
    :maxdepth: 1

    kernel-space
    user-space
