// SPDX-License-Identifier: GPL-2.0-only
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/usermode_driver.h>

DECLARE_WAIT_QUEUE_HEAD(shmem_wait_queue);

static dev_t shmem_dev;
static struct cdev shmem_cdev;
static struct class *umd_class;

static void vma_remap_close(struct vm_area_struct *vma)
{
	void *addr = vma->vm_private_data;

	ClearPageReserved(virt_to_page(addr));
	kfree(addr);
}

static vm_fault_t vma_remap_fault(struct vm_fault *vmf)
{
	struct page *page;
	struct vm_area_struct *vma = vmf->vma;
	void *addr = vma->vm_private_data;

	if (addr) {
		page = virt_to_page(addr);
		get_page(page);
		vmf->page = page;
	}

	return 0;
}

static const struct vm_operations_struct vma_remap_ops = {
	.close = vma_remap_close,
	.fault = vma_remap_fault,
};

static int shmem_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err;
	struct umd_shmem *shm_info = &(current->umd_info->ipc.shm_info);
	void *addr = kzalloc(UMD_SHMEM_LEN, GFP_KERNEL);
	phys_addr_t paddr = virt_to_phys(addr);

	/* make page non-swappable, always on memory after first access */
	SetPageReserved(virt_to_page(addr));

	vma->vm_ops = &vma_remap_ops;
	vma->vm_flags = VM_SHARED | VM_LOCKED | VM_DONTEXPAND | VM_DONTDUMP;
	err = remap_pfn_range(vma, vma->vm_start, paddr >> PAGE_SHIFT,
			      vma->vm_end - vma->vm_start,
			      vma->vm_page_prot);
	if (err)
		return -EAGAIN;

	vma->vm_private_data = addr;
	/* prepare to launch kthreads waiting on shm */
	shm_info->mm_map = addr;
	shm_info->wait_event = 1;
	wake_up(&shmem_wait_queue);
	return 0;
}

static int shmem_open(struct inode *inode, struct file *file)
{
	struct umd_info *umd_info = current->umd_info;

	/* current process not launched by UMH infrastructure*/
	if (!umd_info)
		return -ENODEV;

	if (umd_info->ipc.mode != UMD_IPC_SHMEM)
		return -EINVAL;

	return nonseekable_open(inode, file);
}

static struct file_operations shmem_fops = {
	.open = shmem_open,
	.mmap = shmem_mmap,
	.llseek = no_llseek,
};

int umd_shmem_cdev_create(void)
{
	int err;

	err = alloc_chrdev_region(&shmem_dev, 0, 1, "usermode");
	if (err) {
		pr_err("umd: failed to allocate char dev region\n");
		return -ENOMEM;
	}
	umd_class = class_create(THIS_MODULE, "usermode");
	if (IS_ERR(umd_class)) {
		pr_err("umd: failed to create device class\n");
		err = PTR_ERR(umd_class);
		umd_class = NULL;
		goto out;
	}
	device_create(umd_class, NULL, shmem_dev, NULL, "usermode");
	cdev_init(&shmem_cdev, &shmem_fops);
	err = cdev_add(&shmem_cdev, shmem_dev, 1);
	if (err) {
		pr_err("umd: failed to register char device\n");
		unregister_chrdev_region(shmem_dev, 1);
		goto out_class;
	}

	return 0;
out_class:
	class_destroy(umd_class);
out:
	unregister_chrdev_region(shmem_dev, 1);
	return err;
}

void umd_shmem_cdev_cleanup(void)
{
	if (umd_class) {
		device_destroy(umd_class, shmem_dev);
		class_destroy(umd_class);
	}
	cdev_del(&shmem_cdev);
	unregister_chrdev_region(shmem_dev, 1);
}

static int __init umd_shmem_cdev_init(void)
{
	int err;

	err = umd_shmem_cdev_create();
	if (err) {
		pr_err("umd: failed to create char device: %d", err);
		return err;
	}

	return 0;
}
device_initcall(umd_shmem_cdev_init);
