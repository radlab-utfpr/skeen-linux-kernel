#ifndef __LINUX_SKEEN_H_
#define __LINUX_SKEEN_H_

#include <linux/usermode_driver.h>
#include <linux/list.h>
#include <linux/skeen/protocol.h>

struct skeen_completion_operations;

/*
 * Context related structure and functions
 */
struct skeen_context {
	struct umd_info umd_info; /* general information about umh */

	struct skeen_kreq *kreq; /* request from kernel to us */
	struct skeen_ures *ures; /* response from us to kernel */
	unsigned int timeout; /* how long should polling lasts */

	struct skeen_completion_operations *cop;
};

struct skeen_context * skeen_context_create(const char *name);
int skeen_request_send(struct skeen_context *ctx);
int skeen_request_recv_poll(struct skeen_context *ctx,
			    unsigned long timeout);
void skeen_request_destroy(struct skeen_context *ctx);
#ifdef CONFIG_SKEEN_SHMEM_PGFAULT
bool skeen_request_recv(struct skeen_context *ctx);
#endif

/*
 * Internal kernel interface (callbacks) structure
 */
struct skeen_completion_operations {
	void (*recvd)(struct skeen_context *ctx);
	void (*sent)(struct skeen_context *ctx);
};

#endif /* __LINUX_SKEEN_H_ */
