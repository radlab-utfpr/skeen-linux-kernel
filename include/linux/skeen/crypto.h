/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LINUX_SKEEN_CRYPTO_H
#define _LINUX_SKEEN_CRYPTO_H

#include <linux/skeen.h>
#include <linux/list.h>
#include <linux/completion.h>

typedef enum {
	SKEEN_CRYPTO_OP_SETKEY,
	SKEEN_CRYPTO_OP_ENCRYPT,
	SKEEN_CRYPTO_OP_DECRYPT,
} skeen_crypto_op_t;

struct skeen_crypto_context {
	struct list_head list;
	struct completion completion;

	struct skeen_context *skeen_ctx;
	/* It must be set by the specific alg code */
	void *tfm_ctx;
};

int skeen_crypto_ctx_init(void *tfm_ctx);
void skeen_crypto_ctx_free(void *tfm_ctx);

#endif /* _LINUX_SKEEN_CRYPTO_H */
