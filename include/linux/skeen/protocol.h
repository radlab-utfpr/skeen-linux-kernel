// SPDX-License-Identifier: GPL-2.0
#ifndef _LINUX_SKEEN_PROTOCOL_H
#define _LINUX_SKEEN_PROTOCOL_H

#include <uapi/linux/skeen/protocol.h>

void skeen_proto_set_opcode(struct skeen_kreq *kreq,
			    skeen_proto_opcode_t opcode);
void skeen_proto_set_data(struct skeen_kreq *kreq, const u8 *data,
			  u32 data_len);
bool skeen_proto_check_crc32(struct skeen_ures *ures);
skeen_proto_retval_t skeen_proto_get_retval(struct skeen_ures *ures);
size_t skeen_proto_get_data(struct skeen_ures *ures, u8 *data, size_t len);

#endif /* _LINUX_SKEEN_PROTOCOL_H */
