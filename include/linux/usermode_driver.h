#ifndef __LINUX_USERMODE_DRIVER_H__
#define __LINUX_USERMODE_DRIVER_H__

#include <linux/umh.h>
#include <linux/path.h>

#define UMD_IPC_PIPE	0	/* use pipe as IPC with userspace */
#define UMD_IPC_SHMEM	1	/* use shared memory instead as IPC */

#define UMD_SHMEM_LEN	PAGE_SIZE

extern struct wait_queue_head shmem_wait_queue;

struct umd_shmem {
	void *mm_map;
	int wait_event;
};

int umd_shmem_cdev_create(void);
void umd_shmem_cdev_cleanup(void);

struct umd_ipc {
	int mode;
	struct umd_shmem shm_info;
	struct file *pipe_to_umh;
	struct file *pipe_from_umh;
};

struct umd_info {
	const char *driver_name;
	struct umd_ipc ipc;
	struct path wd;
	struct pid *tgid;
};
int umd_load_blob(struct umd_info *info, const void *data, size_t len);
int umd_unload_blob(struct umd_info *info);
int fork_usermode_driver(struct umd_info *info, int ipc_mode);

#endif /* __LINUX_USERMODE_DRIVER_H__ */
