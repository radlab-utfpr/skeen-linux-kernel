// SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note
#ifndef _LINUX_UAPI_SKEEN_PROTOCOL_H
#define _LINUX_UAPI_SKEEN_PROTOCOL_H

#include <linux/types.h>

#define SKEEN_PROTO_MAX_PAYLOAD_LEN 40
#define SKEEN_PROTO_MAX_DATALEN 32

/* Operations for core functionality.
 * It's used by skeen core infrastructure only, like the healthy check code
 * that is common to all userspace app being instantiated */
typedef enum {
	SKEEN_PROTO_OP_HEALTHY,
	/* Crypto operations code */
	SKEEN_PROTO_CRYPTO_OP_SETKEY,
	SKEEN_PROTO_CRYPTO_OP_ENCRYPT,
	SKEEN_PROTO_CRYPTO_OP_DECRYPT,
	SKEEN_PROTO_OP_TOTAL
} skeen_proto_opcode_t;

typedef enum {
	SKEEN_PROTO_RET_OK,
	SKEEN_PROTO_RET_ERR,
	SKEEN_PROTO_RET_CONT,
	SKEEN_PROTO_RET_TOTAL,
} skeen_proto_retval_t;

/* SKEEN underneath protocol structure.
 * Every other abstraction should fit it */
struct skeen_ures {
	struct {
		__u32 id;
	} header;
	union {
		__u8 data[SKEEN_PROTO_MAX_PAYLOAD_LEN];
		struct {
			skeen_proto_retval_t retval;
			__u32 data_len;
			__u8 data[SKEEN_PROTO_MAX_DATALEN];
		} fields;
	} payload;
	__u8 private[8];
	/* align structure to 64 bits */
	__u8 reserved[12];
}__packed;

struct skeen_kreq {
	struct {
		__u32 id;
	} header;
	union {
		__u8 data[SKEEN_PROTO_MAX_PAYLOAD_LEN];
		struct {
			skeen_proto_opcode_t opcode;
			__u32 data_len;
			__u8 data[SKEEN_PROTO_MAX_DATALEN];
		} fields;
	} payload;
	__u8 private[8];
	/* align structure to 64 bits */
	__u8 reserved[12];
}__packed;

#endif /* _LINUX_UAPI_SKEEN_PROTOCOL_H */
