// SPDX-License-Identifier: GPL-2.0
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/workqueue.h>
#include <linux/wait.h>
#include <linux/mutex.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/usermode_driver.h>
#include <linux/skeen.h>
#include <linux/skeen/protocol.h>

#define SKEEN_US_OFFSET (UMD_SHMEM_LEN >> 1) /* half of shmem size */
#define SKEEN_THREAD_SCHED_INTERVAL 5 /* us */

extern char skeen_umh_start;
extern char skeen_umh_end;

typedef enum skeen_request_state {
	SKEEN_REQ_DEAD,
	SKEEN_REQ_IDLE,
	SKEEN_REQ_RUNNING,
	SKEEN_REQ_RECVD,
	SKEEN_REQ_SENT
} req_state_t;

struct skeen_dispatcher {
	bool wait_event;
	bool done;
};
static struct skeen_dispatcher dispatcher;
DECLARE_WAIT_QUEUE_HEAD(dp_wait_queue);

/* Workqueue to handle recv() through polling mechanism */
static struct workqueue_struct *skeen_wq;

struct skeen_request {
	struct list_head list;
	struct work_struct work;

	struct skeen_context *ctx;
	req_state_t state;
};

LIST_HEAD(request_list);
DEFINE_MUTEX(reqlist_mutex);

/* Send and Recv buffers are kept in separete memory regions, so they can't
 * overlap each other. However, the counterpart of such operation (userspace for
 * "send" and kernel for "recv") are the responsibles for freeing the data
 * structure from the buffer. Also, both buffers are handled as a FIFO circular
 * buffer, with their limits being:
 *
 *	[   send    |    recv   ]
 *	^           ^           ^
 *	0       US_OFFSET   PAGE_SIZE
 */
static void __request_send(struct work_struct *work)
{
	struct skeen_request *req;
	struct skeen_context *ctx;
	struct skeen_kreq *kreq_mem;

	req = container_of(work, struct skeen_request, work);
	ctx = req->ctx;
	kreq_mem = ctx->umd_info.ipc.shm_info.mm_map;

	/* we need to check if the request wasn't prematurely destroyed
	 * before it activelly run */
	if (req->state != SKEEN_REQ_DEAD) {
		memcpy(kreq_mem, ctx->kreq, sizeof(struct skeen_kreq));
		req->state = SKEEN_REQ_SENT;
	}

	dispatcher.wait_event = true;
	wake_up(&dp_wait_queue);
}

int skeen_request_send(struct skeen_context *ctx)
{
	struct skeen_request *req;

	req = kzalloc(sizeof(struct skeen_request), GFP_KERNEL);
	if (!req)
		return -ENOMEM;

	req->ctx = ctx;

	mutex_lock(&reqlist_mutex);
	list_add_tail(&req->list, &request_list);
	mutex_unlock(&reqlist_mutex);

	req->state = SKEEN_REQ_RUNNING;
	INIT_WORK(&req->work, __request_send);
	queue_work(skeen_wq, &req->work);
	return 0;
}

static void __request_recv_poll(struct work_struct *work)
{
	struct skeen_request *req;
	struct skeen_context *ctx;
	struct skeen_ures *ures;
	unsigned long timeout;
	unsigned long data_len = sizeof(struct skeen_ures);
	char zeromem[sizeof(struct skeen_ures)] = {0};
	static unsigned long read_offset = 0;
	bool loop = false;

	req = container_of(work, struct skeen_request, work);
	ctx = req->ctx;
	timeout = ctx->timeout;
	ures = (ctx->umd_info.ipc.shm_info.mm_map + SKEEN_US_OFFSET +
		read_offset);

	if (timeout == 0)
		loop = true;

	do {
		/* requests can be prematurely (before even actually run)
		 * destroyed, because of that we need to check it every time
		 * this request handler polls */
		if (req->state == SKEEN_REQ_DEAD)
			break;

		if (memcmp(ures, zeromem, data_len)) {
			memcpy(ctx->ures, ures, data_len);
			memset(ures, 0, data_len);

			/* remember the shmem is a circular buffer */
			if ((read_offset + data_len) >= SKEEN_US_OFFSET)
				read_offset = 0;
			else
				read_offset += data_len;
			break;
		}

		schedule_timeout_interruptible(
			usecs_to_jiffies(SKEEN_THREAD_SCHED_INTERVAL));
	} while (loop || --timeout);

	if (!loop && timeout == 0)
		ctx->ures = ERR_PTR(-ETIME);

	if (req->state != SKEEN_REQ_DEAD)
		req->state = SKEEN_REQ_RECVD;

	dispatcher.wait_event = true;
	wake_up(&dp_wait_queue);
}

/* Polling recv() function, blocking the execution in a busy-wait manner
 * through resched_timeout(). If that's not you want, check out the
 * _pgfault() variant (not implemented yet). */
int skeen_request_recv_poll(struct skeen_context *ctx,
			    unsigned long timeout)
{
	struct skeen_request *req;

	req = kzalloc(sizeof(struct skeen_request), GFP_KERNEL);
	if (!req)
		return -ENOMEM;

	req->ctx = ctx;
	req->ctx->timeout = timeout;
	req->state = SKEEN_REQ_RUNNING;

	mutex_lock(&reqlist_mutex);
	list_add_tail(&req->list, &request_list);
	mutex_unlock(&reqlist_mutex);

	INIT_WORK(&req->work, __request_recv_poll);
	queue_work(skeen_wq, &req->work);
	return 0;
}

static int skeen_umh_health_check(struct skeen_context *ctx)
{
	struct skeen_request *req;
	struct skeen_context **pctx = &ctx;
	struct skeen_kreq *kreq;
	struct skeen_ures *ures;
	char *kreq_data = "hello";
	char *ures_data;
	int err = 0;

	req = container_of(pctx, struct skeen_request, ctx);

	/* send the healthy data to userspace process */
	kreq = kzalloc(sizeof(struct skeen_kreq), GFP_KERNEL);
	if (!kreq)
		return -ENOMEM;

	skeen_proto_set_opcode(kreq, SKEEN_PROTO_OP_HEALTHY);
	skeen_proto_set_data(kreq, kreq_data, 6);

	/* health check is executed to make sure the userspace process is
	 * actually running, before any other work can actually be
	 * performed, because of that, call the __request_* functions
	 * directly, bypassing workqueue */
	ctx->kreq = kreq;
	__request_send(&req->work);

	/* [debug-mode] poll the memory forever */
	ctx->timeout = 0;
	ures = kzalloc(sizeof(struct skeen_ures), GFP_KERNEL);
	ctx->ures = ures;
	__request_recv_poll(&req->work);
	BUG_ON(PTR_ERR(ures) == -ETIME);

	ures_data = kmalloc(SKEEN_PROTO_MAX_DATALEN, GFP_KERNEL);
	if (!skeen_proto_check_crc32(ures) ||
	    (skeen_proto_get_retval(ures) != SKEEN_PROTO_RET_OK) ||
	    (skeen_proto_get_data(ures, ures_data,
				  SKEEN_PROTO_MAX_DATALEN) != 6)) {
		pr_info("skeen: health check failed: %d %s\n", ures->header.id,
			ures_data);
		err = -1;
		goto out;
	}

	if (!memcmp(ures_data, kreq_data, 6))
		err = -1;
out:
	ctx->kreq = NULL;
	ctx->ures = NULL;
	kfree(kreq);
	kfree(ures);
	return err;
}

static void skeen_context_launch(struct work_struct *work)
{
	struct skeen_request *req;
	struct skeen_context *ctx;
	struct umd_shmem *shm_info;
	int err;

	req = container_of(work, struct skeen_request, work);
	ctx = req->ctx;

	err = umd_load_blob(&ctx->umd_info, &skeen_umh_start,
			    &skeen_umh_end - &skeen_umh_start);
	if (err)
		return;

	fork_usermode_driver(&ctx->umd_info, UMD_IPC_SHMEM);
	pr_info("skeen: userspace program called with pid: %d\n",
		pid_nr(ctx->umd_info.tgid));

	shm_info = &ctx->umd_info.ipc.shm_info;
	wait_event(shmem_wait_queue, shm_info->wait_event != 0);

	if ((err = skeen_umh_health_check(ctx)) != 0) {
		switch (err) {
		case -ENOMEM:
			pr_err("skeen: failed to allocate data\n");
			break;
		case -ETIME:
			pr_err("skeen: failed to receive data in time\n");
			break;
		case 1:
			pr_err("skeen: userspace program not healthy\n");
			break;
		}

		return;
	}

	req->state = SKEEN_REQ_IDLE;
	pr_info("skeen: userspace program initialized with success\n");
}

struct skeen_context * skeen_context_create(const char *name)
{
	int err = 0;
	struct skeen_context *ctx;
	struct skeen_request *req;

	ctx = kzalloc(sizeof(struct skeen_context), GFP_KERNEL);
	if (!ctx)
		return ERR_PTR(-ENOMEM);
	ctx->umd_info.driver_name = name;
	ctx->kreq = kzalloc(sizeof(struct skeen_kreq), GFP_KERNEL);
	if (!ctx->kreq) {
		err = -ENOMEM;
		goto out;
	}
	ctx->ures = kzalloc(sizeof(struct skeen_ures), GFP_KERNEL);
	if (!ctx->ures) {
		err = -ENOMEM;
		kfree(ctx->kreq);
		goto out;
	}

	req = kzalloc(sizeof(struct skeen_request), GFP_KERNEL);
	if (!req) {
		err = -ENOMEM;
		goto out_ctx;
	}
	req->ctx = ctx;

	INIT_WORK(&req->work, skeen_context_launch);
	queue_work(skeen_wq, &req->work);
	return ctx;
out_ctx:
	kfree(ctx->ures);
	kfree(ctx->kreq);
out:
	kfree(ctx);
	return ERR_PTR(err);
}

void skeen_request_destroy(struct skeen_context *ctx)
{
	struct skeen_request *req;

	pr_info("skeen: stopping request for pid %d\n",
		pid_nr(ctx->umd_info.tgid));
	list_for_each_entry_rcu(req, &request_list, list) {
		if (req->ctx == ctx) {
			kfree(ctx->ures);
			kfree(ctx);
			req->state = SKEEN_REQ_DEAD;
		}
	}
}

static void __dispatcher_run(struct work_struct *work)
{
	struct skeen_request *req;
	struct skeen_context *ctx;

	do {
		wait_event(dp_wait_queue, dispatcher.wait_event == true);
		/* dispatcher can only be turned off when the request list
		 * is empty, otherwise requests external costumers can get
		 * stuck */
		if (list_empty(&request_list))
		    break;

		list_for_each_entry_rcu(req, &request_list, list) {
			ctx = req->ctx;
			switch (req->state) {
			case SKEEN_REQ_RECVD:
				ctx->cop->recvd(ctx);
				break;
			case SKEEN_REQ_SENT:
				ctx->cop->sent(ctx);
				break;
			case SKEEN_REQ_DEAD:
				list_del(&req->list);
				break;
			case SKEEN_REQ_RUNNING:
			case SKEEN_REQ_IDLE:
				break;
			}
		}
	} while(1);

	dispatcher.done = true;
}

static bool skeen_dispatcher_init(void)
{
	static DECLARE_WORK(dp_work, __dispatcher_run);

	dispatcher.wait_event = false;
	dispatcher.done = false;
	return queue_work(skeen_wq, &dp_work);
}

static void skeen_reqlist_cleanup(void)
{
	struct skeen_request *req;

	list_for_each_entry(req, &request_list, list) {
		/* list can't be cleaned if there are requests still alive */
		BUG_ON(req->state != SKEEN_REQ_DEAD);
		list_del(&req->list);
	}
}

static int __init skeen_init(void)
{
	struct skeen_context *ctx;

	skeen_wq = alloc_workqueue("skeend", WQ_MEM_RECLAIM | WQ_CPU_INTENSIVE,
				   0);
	if (!skeen_wq)
		return -ENOMEM;

	skeen_dispatcher_init();
	pr_info("skeen: module initialized\n");
#if 1
	/* DEBUG-only: skeen should be used as interface for other subsystems make
	 * use of its features. */
	ctx = skeen_context_create("skeen_debug");
	if (IS_ERR(ctx)) {
		switch (PTR_ERR(ctx)) {
		case -EBUSY:
			pr_err("skeen: umh execution is disabled\n");
			break;
		case -EINVAL:
			pr_err("skeen: invalid exec path\n");
			break;
		case -ENOMEM:
			pr_err("skeen: failed to allocate usermodehelper\n");
			break;
		case -EINTR:
			pr_err("skeen: process interrupted\n");
			break;
		default:
			pr_err("skeen: something yet unknown went wrong\n");
			break;
		}
	}

	return PTR_ERR(ctx);
}
#endif
late_initcall(skeen_init);

static void __exit skeen_exit(void)
{
	skeen_reqlist_cleanup();

	/* make sure dispatcher also ends gracefully */
	dispatcher.wait_event = true;
	wake_up(&dp_wait_queue);
	while (dispatcher.done != true);
	destroy_workqueue(skeen_wq);
	pr_info("skeen: module exited\n");
}
module_exit(skeen_exit);

MODULE_AUTHOR("Bruno Meneguele <bmeneg@redhat.com>");
MODULE_DESCRIPTION("Secure Kernel Execution Environment");
MODULE_LICENSE("GPL");
