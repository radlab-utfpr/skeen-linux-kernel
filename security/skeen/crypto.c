// SPDX-License-Identifier: GPL-2.0
/*
 * Crypto layer abstraction for SKEEN
 */

#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/completion.h>
#include <linux/skeen.h>
#include <linux/skeen/crypto.h>
#include <linux/skeen/protocol.h>

LIST_HEAD(ctx_list);
DECLARE_WAIT_QUEUE_HEAD(completion_wait_queue);

/* Travese our internal context list to find the corresponding
 * skeen_crypto_context */
static struct skeen_crypto_context * get_icontext(void *tfm_ctx,
					struct skeen_context *skeen_ctx)
{
	struct skeen_crypto_context *ictx = NULL;
	int err;

	if (tfm_ctx) {
		list_for_each_entry(ictx, &ctx_list, list) {
			err = memcmp(ictx->tfm_ctx, tfm_ctx,
				     sizeof(*tfm_ctx));
			if (!err)
				return ictx;
		}
	}
	if (!ictx && skeen_ctx) {
		list_for_each_entry(ictx, &ctx_list, list) {
			err = memcmp(ictx->skeen_ctx, skeen_ctx,
				     sizeof(struct skeen_context));
			if (!err)
				return ictx;
		}
	}

	return NULL;
}

static int __crypto_exec_setkey(struct skeen_crypto_context *ctx,
				const u8 *key, unsigned int keylen)
{
	struct skeen_kreq *kreq;
	struct skeen_ures *ures;
	u8 *data = (u8 *)key;
	u32 data_len = (u32)keylen;
	int err = 0;

	kreq = kzalloc(sizeof(struct skeen_kreq), GFP_KERNEL);
	ures = kzalloc(sizeof(struct skeen_ures), GFP_KERNEL);
	ctx->skeen_ctx->kreq = kreq;
	ctx->skeen_ctx->ures = ures;

	skeen_proto_set_opcode(kreq, SKEEN_PROTO_CRYPTO_OP_SETKEY);
	skeen_proto_set_data(kreq, data, data_len);

	err = skeen_request_send(ctx->skeen_ctx);
	if (err)
		return err;

	wait_for_completion(&ctx->completion);
#if 0
	wait_event(completion_wait_queue,
		   ctx->completion_event[SENT_EV_IDX] == true);
#endif
	err = skeen_proto_get_retval(ures);
	return err;
}

static int __crypto_exec_crypt(struct skeen_crypto_context *ctx,
				skeen_crypto_op_t op, u8 *dst, const u8 *src)
{
	struct skeen_kreq *kreq;
	struct skeen_ures *ures;
	u8 *resp, *data = (u8 *)src;
	u32 resp_len, data_len = 16;
	int err = 0;

	kreq = kzalloc(sizeof(struct skeen_kreq), GFP_KERNEL);
	ures = kzalloc(sizeof(struct skeen_ures), GFP_KERNEL);
	ctx->skeen_ctx->kreq = kreq;
	ctx->skeen_ctx->ures = ures;

	if (op == SKEEN_CRYPTO_OP_ENCRYPT)
		skeen_proto_set_opcode(kreq, SKEEN_PROTO_CRYPTO_OP_ENCRYPT);
	else
		skeen_proto_set_opcode(kreq, SKEEN_PROTO_CRYPTO_OP_DECRYPT);

	skeen_proto_set_data(kreq, data, data_len);

	err = skeen_request_send(ctx->skeen_ctx);
	if (err)
		return err;

	/* DEBUG-only: loop forever */
	err = skeen_request_recv_poll(ctx->skeen_ctx, 0);
	if (err)
		return err;

#if 0
	ctx->completion_event[RECVD_EV_IDX] = false;
#endif
	wait_for_completion(&ctx->completion);
#if 0
	wait_event(completion_wait_queue,
		   ctx->completion_event[RECVD_EV_IDX] == true);
#endif

	resp = kmalloc(SKEEN_PROTO_MAX_DATALEN, GFP_KERNEL);
	resp_len = skeen_proto_get_data(ures, resp, SKEEN_PROTO_MAX_DATALEN);
	if (resp_len != data_len) {
		pr_err("skeen-crypto: data length mismatch\n");
		memset(dst, 0, resp_len);
	} else {
		memcpy(dst, resp, resp_len);
	}

	err = skeen_proto_get_retval(ures);
	return err;
}

int skeen_crypto_exec(void *tfm, skeen_crypto_op_t op, void *opdata,
		      void *opdata_ext)
{
	struct skeen_crypto_context *ictx;
	int err = 0;

	ictx = get_icontext(tfm, NULL);
	if (!ictx) {
		err = -EINVAL;
		goto out;
	}

	switch (op) {
	case SKEEN_CRYPTO_OP_SETKEY:
		err = __crypto_exec_setkey(ictx, opdata,
					   *((u32 *)opdata_ext));
		if (err)
			goto out;
		break;
	case SKEEN_CRYPTO_OP_ENCRYPT:
	case SKEEN_CRYPTO_OP_DECRYPT:
		err = __crypto_exec_crypt(ictx, op, opdata, opdata_ext);
		if (err)
			goto out;
		break;
	default:
		err = -ENOTSUPP;
	}

out:
	return err;
}

static void skeen_crypto_req_complete(struct skeen_context *ctx)
{
	struct skeen_crypto_context *ictx;

	ictx = get_icontext(NULL, ctx);
	BUG_ON(ictx == NULL);
	complete(&ictx->completion);
}

static struct skeen_completion_operations complete_ops = {
	.recvd = skeen_crypto_req_complete,
	.sent = skeen_crypto_req_complete
};

#if 0
static void skeen_crypto_req_recvd(struct skeen_context *ctx)
{
	struct skeen_crypto_context *ictx;

	ictx = get_icontext(NULL, ctx);
	BUG_ON(ictx == NULL);
	ictx->completion_event[RECVD_EV_IDX] = true;
	wake_up(&completion_wait_queue);
}

static void skeen_crypto_req_sent(struct skeen_context *ctx)
{
	struct skeen_crypto_context *ictx;

	ictx = get_icontext(NULL, ctx);
	BUG_ON(ictx == NULL);
	ictx->completion_event[SENT_EV_IDX] = true;
	wake_up(&completion_wait_queue);
}

static struct skeen_completion_operations complete_ops = {
	.recvd = skeen_crypto_req_recvd,
	.sent = skeen_crypto_req_sent
};
#endif

int skeen_crypto_ctx_init(void *tfm_ctx)
{
	struct skeen_crypto_context *ictx;

	ictx = kzalloc(sizeof(struct skeen_crypto_context), GFP_KERNEL);
	if (!ictx)
		return -ENOMEM;

	ictx->skeen_ctx = skeen_context_create("skeen_crypto");
	if (IS_ERR(ictx->skeen_ctx))
		return PTR_ERR(ictx->skeen_ctx);

	ictx->skeen_ctx->cop = &complete_ops;
	ictx->tfm_ctx = tfm_ctx;
	init_completion(&ictx->completion);
	list_add_tail(&ictx->list, &ctx_list);

	return 0;
}

void skeen_crypto_ctx_free(void *tfm_ctx)
{
	struct skeen_crypto_context *ictx;

	ictx = get_icontext(tfm_ctx, NULL);
	BUG_ON(ictx == NULL);

	if (ictx->skeen_ctx)
		kfree(ictx->skeen_ctx);

	kfree(ictx);
}
