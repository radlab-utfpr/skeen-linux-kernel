// SPDX-License-Identifier: GPL-2.0
/*
 * SKEEN Crypto AES transform
 */

#include <linux/module.h>
#include <linux/list.h>
#include <linux/types.h>
#include <linux/crypto.h>
#include <linux/skeen/crypto.h>
#include <crypto/aes.h>

static int skeen_aes_setkey(struct crypto_tfm *tfm, const u8 *key,
			    unsigned int keylen)
{
	skeen_crypto_op_t opcode = SKEEN_CRYPTO_OP_SETKEY;
	return skeen_crypto_exec(tfm, opcode, key, keylen);
}

static void skeen_aes_encrypt(struct crypto_tfm *tfm, u8 *dst,
			      const u8 *src)
{
	skeen_crypto_op_t opcode = SKEEN_CRYPTO_OP_ENCRYPT;

	if (skeen_crypto_exec(tfm, opcode, dst, src))
		memset(dst, 0, 16);
}

static void skeen_aes_decrypt(struct crypto_tfm *tfm, u8 *dst,
			      const u8 *src)
{
	skeen_crypto_op_t opcode = SKEEN_CRYPTO_OP_DECRYPT;

	if (skeen_crypto_exec(tfm, opcode, dst, src))
		memset(dst, 0, 16);
}

/* We need a way to link crypto_aes_ctx to our internal skeen_context. It's
 * done with an intermediate context object, skeen_crypto_context, which
 * basically contains both contexts into it and must be initialized before
 * the crypto algo actually is ready for performing any action */
static int skeen_aes_init(struct crypto_tfm *tfm)
{
	struct crypto_aes_ctx *tfm_ctx = crypto_tfm_ctx(tfm);
	int err;

	err = skeen_crypto_ctx_init(tfm_ctx);
	if (err)
		return err;

	return 0;
}

static void skeen_aes_exit(struct crypto_tfm *tfm)
{
	struct crypto_aes_ctx *tfm_ctx = crypto_tfm_ctx(tfm);

	skeen_crypto_ctx_free(tfm_ctx);
}

static struct crypto_alg skeen_aes_alg = {
	.cra_name = "skeen-aes",
	.cra_driver_name = "skeen-aes",
	.cra_priority = 100,
	.cra_flags = CRYPTO_ALG_TYPE_CIPHER,
	.cra_blocksize = AES_BLOCK_SIZE,
	.cra_ctxsize  = sizeof(struct crypto_aes_ctx),
	.cra_module = THIS_MODULE,
	.cra_u = {
		.cipher = {
			.cia_min_keysize = AES_MIN_KEY_SIZE,
			.cia_max_keysize = AES_MAX_KEY_SIZE,
			.cia_setkey = skeen_aes_setkey,
			.cia_encrypt = skeen_aes_encrypt,
			.cia_decrypt = skeen_aes_decrypt,
		}
	},
	.cra_init = skeen_aes_init,
	.cra_exit = skeen_aes_exit,
};

static int __init skeen_aes_mod_init(void)
{
	pr_info("skeen-crypto: registering AES alg\n");
	return crypto_register_alg(&skeen_aes_alg);
}

static void __exit skeen_aes_mod_exit(void)
{
	crypto_unregister_alg(&skeen_aes_alg);
}

late_initcall(skeen_aes_mod_init);
module_exit(skeen_aes_mod_exit);

MODULE_DESCRIPTION("SKEEN AES Crypto Interface");
MODULE_LICENSE("GPL");
MODULE_ALIAS_CRYPTO("skeen-aes");
