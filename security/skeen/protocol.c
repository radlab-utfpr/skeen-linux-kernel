// SPDX-License-Identifier: GPL-2.0

#include <linux/skeen.h>
#include <linux/skeen/protocol.h>
#include <linux/mman.h>
#include <linux/types.h>

/* TODO: instead of heavily using BUG_ON() as assertion make the function
 * return actual errors. */

void skeen_proto_set_opcode(struct skeen_kreq *kreq,
			    skeen_proto_opcode_t opcode)
{
	BUG_ON(opcode >= SKEEN_PROTO_OP_TOTAL);
	BUG_ON(!kreq);

	kreq->payload.fields.opcode = opcode;
}

void skeen_proto_set_data(struct skeen_kreq *kreq, const __u8 *data,
			  __u32 data_len)
{
	BUG_ON(data_len > SKEEN_PROTO_MAX_DATALEN);
	BUG_ON(!kreq);

	memcpy(kreq->payload.fields.data, data, data_len);

	/* If data length is not equal the maximum, add 0s as padding
	 * to data[] */
	if (data_len < SKEEN_PROTO_MAX_DATALEN)
		memset(&kreq->payload.fields.data[data_len], 0,
		       SKEEN_PROTO_MAX_DATALEN - data_len);

	kreq->payload.fields.data_len = data_len;
}

bool skeen_proto_check_crc32(struct skeen_ures *ures)
{
	return true;
}

skeen_proto_retval_t skeen_proto_get_retval(struct skeen_ures *ures)
{
	BUG_ON(!ures);
	BUG_ON(ures->payload.fields.retval >= SKEEN_PROTO_RET_TOTAL);

	return ures->payload.fields.retval;
}

size_t skeen_proto_get_data(struct skeen_ures *ures, __u8 *data, size_t len)
{
	BUG_ON(!ures);
	BUG_ON(!data);
	BUG_ON(len > SKEEN_PROTO_MAX_DATALEN);

	memcpy(data, ures->payload.fields.data, len);
	return ures->payload.fields.data_len;
}
