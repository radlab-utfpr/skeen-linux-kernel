// SPDX-License-Identifier: GPL-2.0
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/mman.h>

#include <linux/skeen/protocol.h>

/* printing helpers */
#ifdef DEBUG
#define pr_dbg(f, fmt, ...) \
	fprintf(f, "skeen_us: %s:%d:: [debug] " fmt, \
	       __func__, __LINE__, ##__VA_ARGS__);
#else
#define pr_dbg(f, fmt, ...) {}
#endif
#define pr_info(f, fmt, ...) \
	fprintf(f, "skeen_us: %s:%d:: " fmt, \
	       __func__, __LINE__, ##__VA_ARGS__);

/* shared memory size is based on page size */
#define UMH_SHM_LEN sysconf(_SC_PAGESIZE)

/* log file reference */
FILE *kmsg_f;

/* we want to keep the ability to late logs, but for the early stages we
 * need to use kmsg, make it easy to reference */
#define k_dbg(fmt, ...) pr_dbg(kmsg_f, fmt, ##__VA_ARGS__);
#define k_info(fmt, ...) pr_info(kmsg_f, fmt, ##__VA_ARGS__);

static int __umh_health_check(void *m)
{
	struct skeen_kreq *kreq = m;
	struct skeen_ures *ures = m + UMH_SHM_LEN/2;
	struct skeen_kreq kreq_healthy;

	kreq_healthy.header.id = 0;
	/* zero all payload content before setting anything */
	memset(kreq_healthy.payload.data, 0, SKEEN_PROTO_MAX_PAYLOAD_LEN);
	kreq_healthy.payload.fields.opcode = SKEEN_PROTO_OP_HEALTHY;
	kreq_healthy.payload.fields.data_len = 6;
	strncpy((char *)kreq_healthy.payload.fields.data, "hello",
		kreq_healthy.payload.fields.data_len);
	memset(kreq_healthy.private, 0, 8);
#if 1
	/* debug only */
	sleep(1);
#endif
	k_dbg("%d %s %s\n", kreq->header.id, kreq->payload.fields.data,
	      kreq->private);
	if (kreq->header.id != kreq_healthy.header.id) {
		k_info("request id doesn't match\n");
		k_dbg("id expected = %d, received = %d\n",
		      kreq_healthy.header.id, kreq->header.id);
		return -1;
	}
	if (memcmp(kreq->payload.data, kreq_healthy.payload.data,
		   SKEEN_PROTO_MAX_PAYLOAD_LEN)) {
		k_info("payload data doesn't match\n");
		k_dbg("data expected = (%d %d %s), received = (%d %d %s)\n",
		      kreq_healthy.payload.fields.opcode,
		      kreq_healthy.payload.fields.data_len,
		      kreq_healthy.payload.fields.data,
		      kreq->payload.fields.opcode,
		      kreq->payload.fields.data_len,
		      kreq->payload.fields.data);
		return -1;
	}

	ures->header.id = kreq->header.id;
	ures->payload.fields.retval = SKEEN_PROTO_RET_OK;
	ures->payload.fields.data_len = kreq->payload.fields.data_len;
	memmove(ures->payload.fields.data, kreq->payload.fields.data,
		ures->payload.fields.data_len);
	memmove(ures->private, kreq->private, 8);

	return 0;
}

static void __umh_shm_mmap(void **m)
{
	char *shm_fn = "/dev/usermode";
	int shm_fd;

	/* Caution: we need to wait until device file is ready before trying
	 * to open it. This process might get initialized even before the
	 * file is ready */
	while (access(shm_fn, F_OK) != 0);

	shm_fd = open(shm_fn, O_RDWR);
	if (shm_fd == -1) {
		k_info("failed to open shared file: %d %s\n", errno,
		       strerror(errno));
		*m = NULL;
		return;
	}

	*m = mmap(NULL, UMH_SHM_LEN, PROT_READ | PROT_WRITE, MAP_SHARED,
		 shm_fd, 0);
	if (m == MAP_FAILED) {
		k_info("failed to mmap shared memory file: %d %s\n", errno,
		       strerror(errno));
		*m = NULL;
	}
	/* fd to the shared memory file can be closed once it's mmap'ed */
	close(shm_fd);
}

static void mem_init(void *m)
{
	int err;

	__umh_shm_mmap(&m);
	if (m == NULL) {
		k_info("failed to mmap shared memory\n");
		return;
	}

	k_info("memory successfully mmap'ed\n");
	err = __umh_health_check(m);
	if (err) {
		k_info("failed to initialize communication\n");
		m = NULL;
		return;
	}

	k_info("communication channel initialized\n");
}

int main(int argc, char *argv[])
{
	const char *kmsg_fn = "/dev/kmsg";
	int err = EXIT_SUCCESS;
	void *mem = NULL;

	kmsg_f = fopen(kmsg_fn, "w");
	if (kmsg_f == NULL)
		return EXIT_FAILURE;

	/* write directly to the file without any buffering */
	setvbuf(kmsg_f, 0, _IONBF, 0);
	k_dbg("exec'ed\n");

	mem_init(mem);
	if (mem == NULL)
		err = EXIT_FAILURE;

	fclose(kmsg_f);
#if 1
	/* only for debugging */
	for (;;) {}
#endif
	if (mem != NULL)
		munmap(mem, UMH_SHM_LEN);
	return err;
}

